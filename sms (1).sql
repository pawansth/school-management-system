-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 21, 2020 at 02:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `id` int(11) NOT NULL,
  `className` varchar(20) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) DEFAULT current_timestamp(),
  `created_at` varchar(255) DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `messageBy` int(11) NOT NULL,
  `messageFor` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` varchar(255) NOT NULL,
  `updated_at` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `parentChild`
--

CREATE TABLE `parentChild` (
  `id` int(11) NOT NULL,
  `parentId` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `classId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `term` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `passMark` varchar(10) NOT NULL,
  `obtainedMark` varchar(10) NOT NULL,
  `fullMark` varchar(10) NOT NULL,
  `attendance` varchar(10) NOT NULL,
  `totalDays` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `sectionName` varchar(30) NOT NULL,
  `classId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `signup`
--

CREATE TABLE `signup` (
  `id` int(11) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `middleName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) NOT NULL,
  `houseNumber` varchar(10) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postcode` varchar(15) NOT NULL,
  `contactNumber` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `updated_at` varchar(255) DEFAULT current_timestamp(),
  `created_at` varchar(255) DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `signup`
--

INSERT INTO `signup` (`id`, `firstName`, `middleName`, `lastName`, `houseNumber`, `street`, `city`, `state`, `postcode`, `contactNumber`, `email`, `password`, `status`, `updated_at`, `created_at`) VALUES
(1, 'Pawan', NULL, 'Shrestha', '10', 'pascoe vale rode', 'glenroy', 'victoria', '3046', '0415034435', 'pawanshrestha569@gmail.com', '$2y$10$hzFdwnAZZh4m1H3wnQE3JO0FCwt/9R3kqlJyKNgFQMjWYFEH9uYaO', 0, '2020-05-14 07:28:32', NULL),
(9, 'Anup', NULL, 'Rahul', '14', 'pasoce vale road', 'glenroy', 'VIC', '3046', '9834561234', 'anup@gmail.com', '$2y$10$5ZwREiCbzJ9e99HQ72tjcuSmGPzQCWnKbu12vwiKHPWM42XKBtH1G', 0, '2020-05-21 07:52:55', '2020-05-21 07:52:55');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `middleName` varchar(30) DEFAULT NULL,
  `lastName` varchar(30) NOT NULL,
  `houseNumber` varchar(10) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postcode` varchar(15) NOT NULL,
  `contactNumber` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `classId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `studiedSubjects`
--

CREATE TABLE `studiedSubjects` (
  `id` int(11) NOT NULL,
  `studentId` int(11) NOT NULL,
  `classId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subjectName` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `classId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL,
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `teacherTeach`
--

CREATE TABLE `teacherTeach` (
  `id` int(11) NOT NULL,
  `teacherId` int(11) NOT NULL,
  `classId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `subjectId` int(11) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `updated_at` varchar(255) NOT NULL DEFAULT current_timestamp(),
  `created_at` varchar(255) NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messageSignupBy` (`messageBy`),
  ADD KEY `messageSignupFor` (`messageFor`);

--
-- Indexes for table `parentChild`
--
ALTER TABLE `parentChild`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parentChildParentId` (`parentId`),
  ADD KEY `parentChildStudentId` (`studentId`);

--
-- Indexes for table `result`
--
ALTER TABLE `result`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resultClassId` (`classId`),
  ADD KEY `resultsStudentId` (`studentId`),
  ADD KEY `resultsSubjectId` (`subjectId`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `resultStudentId` (`studentId`),
  ADD KEY `resultSubjectId` (`subjectId`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sectionClassId` (`classId`);

--
-- Indexes for table `signup`
--
ALTER TABLE `signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studentClassId` (`classId`),
  ADD KEY `studentSectionId` (`sectionId`);

--
-- Indexes for table `studiedSubjects`
--
ALTER TABLE `studiedSubjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `studiedSubjectsStudentId` (`studentId`),
  ADD KEY `studiedSubjectsSubjectId` (`subjectId`),
  ADD KEY `studiedSubjectsClassId` (`classId`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subjectClassId` (`classId`);

--
-- Indexes for table `teacherTeach`
--
ALTER TABLE `teacherTeach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacherTeachTeacherId` (`teacherId`),
  ADD KEY `teacherTeachClassId` (`classId`),
  ADD KEY `teacherTeachSubjectId` (`subjectId`),
  ADD KEY `teacherTeachSectionId` (`sectionId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `parentChild`
--
ALTER TABLE `parentChild`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `result`
--
ALTER TABLE `result`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `signup`
--
ALTER TABLE `signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `studiedSubjects`
--
ALTER TABLE `studiedSubjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teacherTeach`
--
ALTER TABLE `teacherTeach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messageSignupBy` FOREIGN KEY (`messageBy`) REFERENCES `signup` (`id`),
  ADD CONSTRAINT `messageSignupFor` FOREIGN KEY (`messageFor`) REFERENCES `signup` (`id`);

--
-- Constraints for table `parentChild`
--
ALTER TABLE `parentChild`
  ADD CONSTRAINT `parentChildParentId` FOREIGN KEY (`parentId`) REFERENCES `signup` (`id`),
  ADD CONSTRAINT `parentChildStudentId` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`);

--
-- Constraints for table `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `resultClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `resultsStudentId` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `resultsSubjectId` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `resultStudentId` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `resultSubjectId` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sectionClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`);

--
-- Constraints for table `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `studentClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `studentSectionId` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`);

--
-- Constraints for table `studiedSubjects`
--
ALTER TABLE `studiedSubjects`
  ADD CONSTRAINT `studiedSubjectsClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `studiedSubjectsStudentId` FOREIGN KEY (`studentId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `studiedSubjectsSubjectId` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjectClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`);

--
-- Constraints for table `teacherTeach`
--
ALTER TABLE `teacherTeach`
  ADD CONSTRAINT `teacherTeachClassId` FOREIGN KEY (`classId`) REFERENCES `class` (`id`),
  ADD CONSTRAINT `teacherTeachSectionId` FOREIGN KEY (`sectionId`) REFERENCES `sections` (`id`),
  ADD CONSTRAINT `teacherTeachSubjectId` FOREIGN KEY (`subjectId`) REFERENCES `subjects` (`id`),
  ADD CONSTRAINT `teacherTeachTeacherId` FOREIGN KEY (`teacherId`) REFERENCES `signup` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
