<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});
// Route for signup
Route::resource('/register', 'SignupController');
Route::get('/register-search', 'SignupController@search');
// Route for signup
Route::resource('/students', 'StudentsController');
Route::get('/students-search', 'StudentsController@search');
Route::get('/students-load-data', 'StudentsController@loadData');
// Route for class
Route::resource('/class', 'ClassModelController');
Route::get('/class-search', 'ClassModelController@search');
// Route for sections
Route::resource('/sections', 'SectionsController');
Route::get('/sections-search', 'SectionsController@search');
// Route for subjects
Route::resource('/subjects', 'SubjectsController');
Route::get('/subjects-search', 'SubjectsController@search');
// Route for teacherTeach
Route::resource('/teach', 'TeacherTeachController');
Route::get('/teach-search', 'TeacherTeachController@search');
Route::get('/teach-load-data', 'TeacherTeachController@loadData');
// Route for parentChild
Route::resource('/parent-child', 'ParentChildController');
Route::get('/parent-child-search', 'ParentChildController@search');
Route::get('/parent-child-load-data', 'ParentChildController@loadData');
// Route for studied subject of students
Route::resource('/studied-subjects', 'StudiedSubjectsController');
Route::get('/studied-subjects-search', 'StudiedSubjectsController@search');
Route::get('/studied-subjects-load-data', 'StudiedSubjectsController@loadData');

Auth::routes(['register' => false]);
Route::post('/custome-login', 'CustomeLogin@login');

// Route::get('/home', 'HomeController@index')->name('home');

// Route for student result
Route::resource('/result', 'ResultModelController');
Route::get('/result-search', 'ResultModelController@search');
Route::get('/result-subjects-load-data', 'ResultModelController@loadData');

// Route for parent view
Route::get('/parent', 'ParentController@index');
Route::get('/view-result/{id}', 'ParentController@viewResult');

// Routes for messages
Route::get('/teacher-list/{id}','MessagesController@teacherList');
Route::get('/message-teacher/{id}','MessagesController@messageTeacher');
Route::post('/send-message', 'MessagesController@sendMessage');
Route::get('/teacher-message-view','TeacherMessageController@messageList');
Route::get('/teacher-message/{id}','TeacherMessageController@teacherMessage');
Route::post('/send-message-parent', 'TeacherMessageController@sendMessage');