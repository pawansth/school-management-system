<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sections extends Model
{
    protected $table = 'sections';

    public function teacherTeach()
    {
        return $this->hasMany('App\teacherTeach', 'sectionId');
    }
    public function classModel()
    {
        return $this->belongsTo('App\classModel');
    }
    public function students()
    {
        return $this->hasMany('App\students', 'foreign_key', 'local_key');
    }
}
