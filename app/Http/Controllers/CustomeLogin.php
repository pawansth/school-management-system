<?php

namespace App\Http\Controllers;

use App\signup;

use Illuminate\Http\Request;
use Auth;

class CustomeLogin extends Controller
{
    public function login(Request $request) {
        if(Auth::attempt([
            'email' => $request->input('email'),
             'password' => $request->input('password')
             ])) {
                 $user = signup::where([
                     'email' => $request->input('email')
                 ])->first();
                 if($user->status == 0) {
                     return redirect('/register');
                 } elseif($user->status == 1) {
                    return redirect('/result');
                 } else {
                    return redirect('/parent');
                 }
             } else {
                 return redirect('/login')->with('error','email or password is worng...');
             }
    }
    
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
      }
}
