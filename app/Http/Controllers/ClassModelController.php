<?php

namespace App\Http\Controllers;

use App\classModel;
use App\sections;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClassModelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = classModel::paginate(10);
        $data = [
            'title' => 'Class',
            'datas' => $datas
        ];

        return view('admin.class.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Add Class',
        ];
        return view('admin.class.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'class'=>'required'
        ]);
        $classModel = new classModel();
        $classModel->className = $request->input('class');
        $classModel->save();
        return redirect('/class')->with('success','Class Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\classModel  $classModel
     * @return \Illuminate\Http\Response
     */
    public function show(classModel $classModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\classModel  $classModel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas = classModel::find($id);
        $data = [
            'title' => 'Update '.$datas['className'],
            'data' => $datas
        ];
        return view('admin.class.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\classModel  $classModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'class'=>'required'
        ]);
        $classModel = classModel::find($id);
        $classModel->className = $request->input('class');
        $classModel->save();
        return redirect('/class')->with('success','Class Updated Successfully....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\classModel  $classModel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::delete('delete from teacherTeach where classId = ?',[$id]);
            DB::delete('delete from studiedSubjects where classId = ?',[$id]);
            DB::delete('delete from result where classId = ?',[$id]);
            DB::delete('delete from subjects where classId = ?',[$id]);
            DB::delete('delete from students where classId = ?',[$id]);
            DB::delete('delete from sections where classId = ?',[$id]);
            classModel::find($id)->delete();
            
            return redirect('/class')->with('success','Deleted Successfully');
        } catch(Exception $e) {
            return redirect('/class')->with('error','Failed to Delete....');
        }
    }
    public function search(Request $search)
    {
        // return $search;
        $datas = DB::table('class')->where('className', 'like', '%'.$search->get('search').'%')->paginate(10);
        $data = [
            'title' => 'Class Search Result',
            'datas' => $datas
        ];
        return view('admin.class.index')->with('data', $data);
    }
        
}
