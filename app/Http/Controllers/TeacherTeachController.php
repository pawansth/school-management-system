<?php

namespace App\Http\Controllers;

use App\teacherTeach;
use App\classModel;
use App\sections;
use App\subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TeacherTeachController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = teacherTeach::paginate(10);
        // $datas = DB::table('teacherTeach')
        //                     ->select('teacherTeach.*','class.className')
        //                     ->join('class', 'class.id','=','teacherTeach.classId')
        //                     ->orderBy('className')
        //                     ->paginate(10);
        $data = [
            'title' => 'Teacher Teaching Subjects',
            'datas' => $datas
        ];

        return view('admin.teacherTeach.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teacherData = DB::table('signup')
                            ->where('status',1)
                            ->orderBy('firstName')
                            ->get();
                            // return $teacherData;
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Teaching Subject',
            'teacherData' => $teacherData,
            'classData' => $classData
        ];
        return view('admin.teacherTeach.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'teacher'=>'required',
            'class'=>'required',
            'section' => 'required',
            'subject'=>'required'
        ]);
        $test = DB::table('teacherTeach')->where(
            'classId',$request->input('class')
            )
            ->where('subjectId',$request->input('subject'))
            ->where('teacherId', $request->input('teacher'))
            ->where('sectionId', $request->input('section'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/teach')->with('error','Data already exist....');
        }
        $teacherTeach = new teacherTeach();
        $teacherTeach->teacherId = $request->input('teacher');
        $teacherTeach->classId = $request->input('class');
        $teacherTeach->sectionId = $request->input('section');
        $teacherTeach->subjectId = $request->input('subject');
        $teacherTeach->save();
        return redirect('/teach')->with('success','Teacher Teaching Subject Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\teacherTech  $teacherTech
     * @return \Illuminate\Http\Response
     */
    public function show(teacherTech $teacherTech)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\teacherTech  $teacherTech
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacherData = DB::table('signup')
                            ->where('status',1)
                            ->orderBy('firstName')
                            ->get();
                            // return $teacherData;
        $classData = classModel::orderBy('className')->get();
        $datas = teacherTeach::where('id',$id)->get();
        $data = [
            'title' => 'Update Teahing Subject',
            'teacherData' => $teacherData,
            'datas' => $datas,
            'classData' => $classData
        ];
        return view('admin.teacherTeach.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\teacherTech  $teacherTech
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'teacher'=>'required',
            'class'=>'required',
            'section' => 'required',
            'subject'=>'required'
        ]);
        $test = DB::table('teacherTeach')->where(
            'classId',$request->input('class')
            )
            ->where('subjectId',$request->input('subject'))
            ->where('teacherId', $request->input('teacher'))
            ->where('sectionId', $request->input('section'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/teach')->with('error','Data already exist can not modify....');
        }
        $teacherTeach = teacherTeach::find($id);
        $teacherTeach->teacherId = $request->input('teacher');
        $teacherTeach->classId = $request->input('class');
        $teacherTeach->sectionId = $request->input('section');
        $teacherTeach->subjectId = $request->input('subject');
        $teacherTeach->save();
        return redirect('/teach')->with('success','Successfully Updated....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\teacherTech  $teacherTech
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        teacherTeach::find($id)->delete();
        return redirect('/teach')->with('success','Successfully Deleted....');
    }

    public function loadData(Request $request) {
        // return $request->input('id');
        $sectionData = DB::table('sections')
                        ->select('id','sectionName')
                        ->where('classId', $request->input('id'))
                        ->get();
        $subjectData = DB::table('subjects')
                        ->select('id','subjectName')
                        ->where('classId', $request->input('id'))
                        ->orderBy('classId')->get();
        $result = [
            'sectionData' => $sectionData,
            'subjectData' => $subjectData
        ];
        return $result;
    }

    public function search(Request $search)
    {
        $datas = DB::table('teacherTeach')
                ->select('teacherTeach.*','signup.firstName')
                ->join('signup', 'signup.id','=', 'teacherTeach.teacherId')
                ->where('firstName','like','%'.$search->get('search').'%')
                ->get();
                // return $datas[0]->id;
        $searchData = teacherTeach::find($datas[0]->id)->paginate(10);
        // return $searchData;
        $data = [
            'title' => 'Teacher Teaching Subjects',
            'datas' => $searchData
        ];

        return view('admin.teacherTeach.index')->with('data', $data);
    }
}
