<?php

namespace App\Http\Controllers;

use App\parentChild;
use App\signup;
use App\messages;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 0) {
                return redirect('/register')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    //
    public function teacherList($id) {
        $teacherList = parentChild::where([
            'parentId' => $id
        ])->get();
        // dd($teacherList->students);
        $data = [
            'title' => 'Teachers',
            'teacherList' => $teacherList,
            'userId' => Auth::user()->id
        ];
        return view('front.parent.teacherList')->with('data', $data);
    }

    public function messageTeacher($id) {
        $messages = messages::where([
            'messageBy'=> Auth::user()->id
        ])
        ->orWhere([
            'messageFor'=> Auth::user()->id
        ])->get();
        $data = [
            'title' => 'Teachers',
            'messages' => $messages,
            'userId' => Auth::user()->id,
            'teacherId' => $id
        ];
        return view('front.parent.message')->with('data', $data);
    }

    public function sendMessage(Request $request) {
        $this->validate($request,[
            'messageToTeacher'=>'required'
        ]);
        $messages = new messages();
        $messages->messageBy = Auth::user()->id;
        $messages->messageFor = $request->input('teacherId');
        $messages->message = $request->input('messageToTeacher');
        $messages->save();

        $messages = messages::where([
            'messageBy'=> Auth::user()->id
        ])
        ->orWhere([
            'messageFor'=> Auth::user()->id
        ])->get();
        $data = [
            'title' => 'Teachers',
            'messages' => $messages,
            'userId' => Auth::user()->id,
            'teacherId' => $request->input('teacherId')
        ];
        return redirect('/message-teacher/'.$request->input('teacherId'))->with('data', $data);
    }
}
