<?php

namespace App\Http\Controllers;

use App\signup;
use App\parentChild;
use App\ResultModel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ParentController extends Controller {

    // Constructor to check if the user is parent or not
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 0) {
                return redirect('/register')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }

    public function index() {
        $children = parentChild::where([
            'parentId' => Auth::user()->id
        ])->get();
        // dd($children[0]);
        // $result = ResultModel::where([
        //     'studentId' => $children[0]->studentId,
        //     'classId' => $children[0]->classId
        // ])->get();
        $data = [
            'title' => 'Children Result',
            'children' => $children,
            'userId' => Auth::user()->id
        ];
        return view('front.parent.index')->with('data', $data);
    }

    public function viewResult($id) {
        $result = ResultModel::where([
            'studentId' => $id
        ])->get();
        $data = [
            'title' => 'Children Result',
            'result' => $result
        ];
        return view('front.parent.viewResult')->with('data', $data);
    }


}
?>