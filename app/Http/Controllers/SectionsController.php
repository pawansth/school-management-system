<?php

namespace App\Http\Controllers;

use App\sections;
use App\classModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SectionsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('sections')
                            ->select('sections.*','class.className')
                            ->join('class', 'class.id','=','sections.classId')
                            ->orderBy('className')
                            ->paginate(10);
        $data = [
            'title' => 'Sections',
            'datas' => $datas
        ];

        return view('admin.sections.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Sections',
            'classData' => $classData
        ];
        return view('admin.sections.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'class'=>'required',
            'section'=>'required'
        ]);
        $test = DB::table('sections')->where(
            'classId',$request->input('class')
            )
            ->where('sectionName',$request->input('section'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/sections')->with('error','Class And Section Already Exists....');
        }
        $sections = new sections();
        $sections->classId = $request->input('class');
        $sections->sectionName = $request->input('section');
        $sections->save();
        return redirect('/sections')->with('success','Section Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sections  $sections
     * @return \Illuminate\Http\Response
     */
    public function show(sections $sections)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sections  $sections
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return $id;
        $classData = classModel::orderBy('className')->get();
        $datas = DB::table('sections')
                            ->select('sections.*','class.className')
                            ->join('class', 'class.id','=','sections.classId')
                            ->where('sections.id', $id)
                            ->get();
        $data = [
            'title' => 'Update class '.$datas[0]->className.' section '.$datas[0]->sectionName,
            'classData' => $classData,
            'data' => $datas
        ];
        return view('admin.sections.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sections  $sections
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'class'=>'required',
            'section'=>'required'
        ]);
        $test = DB::table('sections')->where(
            'classId',$request->input('class')
            )
            ->where('sectionName',$request->input('section'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/sections')->with('error','Class And Section Already Exists....');
        }
        $sections = sections::find($id);
        $sections->classId = $request->input('class');
        $sections->sectionName = $request->input('section');
        $sections->save();
        return redirect('/sections')->with('success','Section Updated Successfully....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sections  $sections
     * @return \Illuminate\Http\Response
     */
    public function destroy(sections $id)
    {
        DB::delete('delete from teacherTeach where sectionId = ?',[$id]);
        DB::delete('delete from students where sectionId = ?',[$id]);
        $data = sections::find($id);
        $data->delete();
        return redirect('/sections')->with('success','Deleted Successfully');
    }
    public function search(Request $search)
    {
        // return $search;
        $datas = DB::table('sections')
                            ->select('sections.*','class.className')
                            ->join('class', 'class.id','=','sections.classId')
                            ->where('class.className',$search->get('search'))
                            ->orderBy('className')
                            ->paginate(10);
        $data = [
            'title' => 'Sections Search Result',
            'datas' => $datas
        ];
        return view('admin.class.index')->with('data', $data);
    }
}
