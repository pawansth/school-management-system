<?php

namespace App\Http\Controllers;

use App\studiedSubjects;
use App\classModel;
use App\subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class StudiedSubjectsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = studiedSubjects::paginate(10);
        $data = [
            'title' => 'Select Student Study Subject',
            'datas' => $datas
        ];
        return view('admin.studiedSubjects.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Studing Subject',
            'classData' => $classData
        ];
        return view('admin.studiedSubjects.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'class'=>'required',
            'student'=>'required',
            'subject'=>'required'
        ]);
        $test = DB::table('studiedSubjects')->where([
            'classId' => $request->input('class'),
            'studentId' => $request->input('student'),
            'subjectId' => $request->input('subject')
        ])->get();
        if(sizeOf($test)>0) {
            return redirect('/studied-subjects')->with('error','Data already exist....');
        }
        $studiedSubjects = new studiedSubjects();
        $studiedSubjects->studentId = $request->input('student');
        $studiedSubjects->classId = $request->input('class');
        $studiedSubjects->subjectId = $request->input('subject');
        $studiedSubjects->save();
        return redirect('/studied-subjects')->with('success','Record Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function show(studiedSubjects $studiedSubjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classData = classModel::orderBy('className')->get();
        $datas = studiedSubjects::where([
            'id' => $id
        ])->get();
        $data = [
            'title' => 'Update Studing Subject',
            'classData' => $classData,
            'datas' => $datas
        ];
        return view('admin.studiedSubjects.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'class'=>'required',
            'student'=>'required',
            'subject'=>'required'
        ]);
        $test = DB::table('studiedSubjects')->where([
            'classId' => $request->input('class'),
            'studentId' => $request->input('student'),
            'subjectId' => $request->input('subject')
        ])->get();
        if(sizeOf($test)>0) {
            return redirect('/studied-subjects')->with('error','Data already exist....');
        }
        $studiedSubjects = studiedSubjects::find($id);
        $studiedSubjects->studentId = $request->input('student');
        $studiedSubjects->classId = $request->input('class');
        $studiedSubjects->subjectId = $request->input('subject');
        $studiedSubjects->save();
        return redirect('/studied-subjects')->with('success','Record Updated Successfully....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        studiedSubjects::find($id)->delete();
        return redirect('/studied-subjects')->with('success','Successfully Deleted....');
    }

    public function loadData(Request $request) {
        // return $request->input('id');

        $studentsData = DB::table('students')
                        ->select('id', 'firstName', 'middleName', 'lastName')
                        ->where([
                            'classId' => $request->input('id')
                        ])
                        ->orderBy('firstName')->get();

        $subjectData = DB::table('subjects')
                        ->select('id','subjectName')
                        ->where('classId', $request->input('id'))
                        ->orderBy('subjectName')->get();
        $result = [
            'studentsData' => $studentsData,
            'subjectData' => $subjectData
        ];
        return $result;
    }

    public function search(Request $search)
    {
        $datas = studiedSubjects::whereHas('students', function(Builder $query) use($search) {
                    $query->where('firstName','like','%'.$search->input('search').'%');
                })->paginate(10);
                // return $datas;
        // $searchData = teacherTeach::find($datas[0]->id)->paginate(10);
        // return $searchData;
        $data = [
            'title' => 'Search Result',
            'datas' => $datas
        ];

        return view('admin.studiedSubjects.index')->with('data', $data);
    }
}
