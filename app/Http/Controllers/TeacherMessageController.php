<?php

namespace App\Http\Controllers;

use App\ResultModel;
use App\classModel;
use App\subjects;
use App\messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class TeacherMessageController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 0) {
                return redirect('/register')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }

    public function messageList() {
        $messages = messages::where([
            'messageFor'=> Auth::user()->id
        ])->groupBy('messageBy')->get();
        $data = [
            'title' => 'Teachers',
            'messages' => $messages,
            'userId' => Auth::user()->id
        ];
        return view('admin.message.viewMessage')->with('data', $data);
    }

    public function teacherMessage($id) {
        $messages = messages::where([
            'messageBy'=> Auth::user()->id
        ])
        ->orWhere([
            'messageFor'=> Auth::user()->id
        ])->get();
        $data = [
            'title' => 'Message',
            'messages' => $messages,
            'userId' => Auth::user()->id,
            'parentId' => $id
        ];
        return view('admin.message.replayMessage')->with('data', $data);
    }

    public function sendMessage(Request $request) {
        $this->validate($request,[
            'messageToTeacher'=>'required'
        ]);
        $messages = new messages();
        $messages->messageBy = Auth::user()->id;
        $messages->messageFor = $request->input('parentId');
        $messages->message = $request->input('messageToTeacher');
        $messages->save();

        $messages = messages::where([
            'messageBy'=> Auth::user()->id
        ])
        ->orWhere([
            'messageFor'=> Auth::user()->id
        ])->get();
        $data = [
            'title' => 'Message',
            'messages' => $messages,
            'userId' => Auth::user()->id,
            'parentId' => $request->input('parentId')
        ];
        return redirect('/teacher-message/'.$request->input('parentId'))->with('data', $data);
    }

}
?>