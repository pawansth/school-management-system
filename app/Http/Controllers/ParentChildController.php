<?php

namespace App\Http\Controllers;

use App\parentChild;
use App\students;
use App\signup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ParentChildController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = parentChild::paginate(10);
        $data = [
            'title' => 'Childrens',
            'datas' => $datas
        ];

        return view('admin.parentChild.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parentData = DB::table('signup')
                            ->where('status',2)
                            ->orderBy('firstName')
                            ->get();
                            // return $teacherData;
        $studentData = students::orderBy('firstName')->get();
        $data = [
            'title' => 'Add Teaching Subject',
            'parentData' => $parentData,
            'studentData' => $studentData
        ];
        return view('admin.parentChild.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'parent'=>'required',
            'child'=>'required'
        ]);
        $test = DB::table('parentChild')
                ->where([
                    'parentId'=>$request->input('parent'),
                    'studentId' => $request->input('child')
                ])
                ->get();
        if(sizeof($test) >0) {
            return redirect('/parent-child')->with('error','Data already exist....');
        }
        $parentChild = new parentChild();
        $parentChild->parentId = $request->input('parent');
        $parentChild->studentId = $request->input('child');
        $parentChild->save();
        return redirect('/parent-child')->with('success','Successfully Added....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\parentChild  $parentChild
     * @return \Illuminate\Http\Response
     */
    public function show(parentChild $parentChild)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\parentChild  $parentChild
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parentData = DB::table('signup')
                            ->where('status',2)
                            ->orderBy('firstName')
                            ->get();
                            // return $teacherData;
        $studentData = students::orderBy('firstName')->get();
        $datas = parentChild::find($id);
        $data = [
            'title' => 'Add Teaching Subject',
            'parentData' => $parentData,
            'studentData' => $studentData,
            'datas' => $datas
        ];
        return view('admin.parentChild.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\parentChild  $parentChild
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'parent'=>'required',
            'child'=>'required'
        ]);
        $test = DB::table('parentChild')
                ->where([
                    'parentId'=>$request->input('parent'),
                    'studentId' => $request->input('child')
                ])
                ->get();
        if(sizeof($test) >0) {
            return redirect('/parent-child')->with('error','Data already exist....');
        }
        $parentChild = parentChild::find($id);
        $parentChild->parentId = $request->input('parent');
        $parentChild->studentId = $request->input('child');
        $parentChild->save();
        return redirect('/parent-child')->with('success','Successfully Updated....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\parentChild  $parentChild
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        parentChild::find($id)->delete();
        return redirect('/parent-child')->with('success','Successfully Deleted....');
    }

    public function search(Request $search) {
        $searchKey = $search->get('search');
        // $datas = DB::table('signup')
        //         ->select('signup.*')
        //         ->join('parentChild','parentChild.parentId','=','signup.id')
        //         ->where('signup.status', 2)
        //         ->where('signup.firstName', 'like', '%'.$searchKey.'%')
        //         ->paginate(10);
        $results = DB::select(
            "SELECT t1.*,students.firstName as sfn,students.middleName as smn,students.lastName as sln FROM ((SELECT parentChild.id, signup.firstName,signup.middleName,signup.lastName,parentChild.parentId,parentChild.studentId FROM signup JOIN parentChild ON parentChild.parentId = signup.id WHERE signup.firstName LIKE '%$searchKey%') as t1) JOIN students ON students.id = t1.id"
        );
        // return $results;
        $data = [
            'title' => 'Childrens',
            'datas' => $results
        ];

        return view('admin.parentChild.form')->with('data', $data);
    }
}
