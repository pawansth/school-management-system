<?php

namespace App\Http\Controllers;

use App\signup;
use App\teacherTeach;
use App\parentChild;
use App\messages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SignupController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = signup::paginate(10);
        $data = [
            'title' => 'Registration',
            'datas' => $datas
        ];

        return view('admin.signup.index')->with('data', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'Add Registration',
        ];
        return view('admin.signup.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'fName'=>'required',
            'lName'=>'required',
            'hNumber'=>'required',
            'streetName'=>'required',
            'cityName' => 'required',
            'state' => 'required',
            'postCodeNumber'=>'required',
            'contactNumber' => 'required',
            'email'=>'required',
            'password'=>'required',
            'status'=>'required'
        ]);
        $signup = new signup();
        $signup->firstName = $request->input('fName');
        $signup->middleName = $request->input('mName');
        $signup->lastName = $request->input('lName');
        $signup->houseNumber = $request->input('hNumber');
        $signup->street = $request->input('streetName');
        $signup->city = $request->input('cityName');
        $signup->state = $request->input('state');
        $signup->postcode = $request->input('postCodeNumber');
        $signup->contactNumber = $request->input('contactNumber');
        $signup->email = $request->input('email');
        $signup->password = bcrypt($request->input('password'));
        $signup->status = $request->input('status');
        $signup->save();
    return redirect('/register')->with('success','User Registered');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function show(signup $signup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $datas = signup::find($id);
        $data = [
            'title' => 'Update '.$datas['firstName'],
            'data' => $datas
        ];
        return view('admin.signup.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fName'=>'required',
            'lName'=>'required',
            'hNumber'=>'required',
            'streetName'=>'required',
            'cityName' => 'required',
            'state' => 'required',
            'postCodeNumber'=>'required',
            'contactNumber' => 'required',
            'email'=>'required',
            'password'=>'required',
            'status'=>'required'
        ]);
        $signup = signup::find($id);
        $signup->firstName = $request->input('fName');
        $signup->middleName = $request->input('mName');
        $signup->lastName = $request->input('lName');
        $signup->houseNumber = $request->input('hNumber');
        $signup->street = $request->input('streetName');
        $signup->city = $request->input('cityName');
        $signup->state = $request->input('state');
        $signup->postcode = $request->input('postCodeNumber');
        $signup->contactNumber = $request->input('contactNumber');
        $signup->email = $request->input('email');
        $signup->password = bcrypt($request->input('password'));
        $signup->status = $request->input('status');
        $signup->save();
        return redirect('/register')->with('success','User Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\signup  $signup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        teacherTeach::where('teacherId', $id)->delete();
        parentChild::where('parentId', $id)->delete();
        messages::where('messageBy', $id)->orWhere('messageFor', $id)->delete();
        signup::find($id)->delete();
        return redirect('/register')->with('success','Delete Successfully');
    }
    public function search(Request $search)
    {
        // return $search;
        $datas = DB::table('signup')->where('firstName', 'like', '%'.$search->get('search').'%')->paginate(10);
        $data = [
            'title' => 'Registration Search Result',
            'datas' => $datas
        ];

        return view('admin.signup.index')->with('data', $data);
    }
}
