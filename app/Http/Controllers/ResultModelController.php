<?php

namespace App\Http\Controllers;

use App\ResultModel;
use App\classModel;
use App\subjects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class ResultModelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = resultModel::paginate(10);
        $data = [
            'title' => 'Result of Students',
            'datas' => $datas
        ];
        return view('admin.result.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Result',
            'classData' => $classData
        ];
        return view('admin.result.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'class'=>'required',
            'student'=>'required',
            'subject'=>'required',
            'result' => 'required|file|mimes:pdf|max:5000',
            'term' => 'required|numeric|max:4'
        ]);
        $path = '';
        $test = DB::table('result')->where([
            'classId' => $request->input('class'),
            'studentId' => $request->input('student'),
            'subjectId' => $request->input('subject'),
            'term' => $request->input('term')
        ])->get();
        if(sizeOf($test)>0) {
            return redirect('/result')->with('error','Data already exist....');
        }
        
        $ResultModel = new ResultModel();
        $ResultModel->studentId = $request->input('student');
        $ResultModel->classId = $request->input('class');
        $ResultModel->subjectId = $request->input('subject');
        $ResultModel->term = $request->input('term');
        $ResultModel->path = $request->file('result')->store('uploads','public');
        $ResultModel->save();
        return redirect('/result')->with('success','Record Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function show(studiedSubjects $studiedSubjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classData = classModel::orderBy('className')->get();
        $datas = ResultModel::where([
            'id' => $id
        ])->get();
        $data = [
            'title' => 'Update Result',
            'classData' => $classData,
            'datas' => $datas
        ];
        return view('admin.result.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'class'=>'required',
            'student'=>'required',
            'subject'=>'required',
            'result' => 'required|file|mimes:pdf|max:5000',
            'term' => 'required|numeric|max:4'
        ]);
        // $path = '';
        // $test = DB::table('result')->where([
        //     'classId' => $request->input('class'),
        //     'studentId' => $request->input('student'),
        //     'subjectId' => $request->input('subject'),
        //     'term' => $request->input('term')
        // ])->get();
        // if(sizeOf($test)>0) {
        //     return redirect('/result')->with('error','Data already exist....');
        // }
        
        $ResultModel = ResultModel::find($id);
        $ResultModel->studentId = $request->input('student');
        $ResultModel->classId = $request->input('class');
        $ResultModel->subjectId = $request->input('subject');
        $ResultModel->term = $request->input('term');
        $ResultModel->path = $request->file('result')->store('uploads','public');
        $ResultModel->save();
        return redirect('/result')->with('success','Record Updated Successfully....');
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\studiedSubjects  $studiedSubjects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ResultModel::find($id)->delete();
        return redirect('/result')->with('success','Successfully Deleted....');
    }

    public function loadData(Request $request) {
        // return $request->input('id');

        $studentsData = DB::table('students')
                        ->select('id', 'firstName', 'middleName', 'lastName')
                        ->where([
                            'classId' => $request->input('id')
                        ])
                        ->orderBy('firstName')->get();

        $subjectData = DB::table('subjects')
                        ->select('id','subjectName')
                        ->where('classId', $request->input('id'))
                        ->orderBy('subjectName')->get();
        $result = [
            'studentsData' => $studentsData,
            'subjectData' => $subjectData
        ];
        return $result;
    }

    public function search(Request $search)
    {
        $datas = ResultModel::whereHas('students', function(Builder $query) use($search) {
                    $query->where('firstName','like','%'.$search->input('search').'%');
                })->paginate(10);
                // return $datas;
        // $searchData = teacherTeach::find($datas[0]->id)->paginate(10);
        // return $searchData;
        $data = [
            'title' => 'Search Result',
            'datas' => $datas
        ];

        return view('admin.result.index')->with('data', $data);
    }
}
