<?php

namespace App\Http\Controllers;

use App\subjects;
use App\classModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SubjectsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = DB::table('subjects')
                            ->select('subjects.*','class.className')
                            ->join('class', 'class.id','=','subjects.classId')
                            ->orderBy('className')
                            ->paginate(10);
        $data = [
            'title' => 'Subjects',
            'datas' => $datas
        ];

        return view('admin.subjects.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Subjects',
            'classData' => $classData
        ];
        return view('admin.subjects.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'class'=>'required',
            'subject'=>'required'
        ]);
        $test = DB::table('subjects')->where(
            'classId',$request->input('class')
            )
            ->where('subjectName',$request->input('subject'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/subjects')->with('error','Class And Subject Already Exists....');
        }
        $subjects = new subjects();
        $subjects->classId = $request->input('class');
        $subjects->subjectName = $request->input('subject');
        $subjects->description = $request->input('description');
        $subjects->save();
        return redirect('/subjects')->with('success','Subject Created Successfully....');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function show(subjects $subjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // return $id;
        $classData = classModel::orderBy('className')->get();
        $datas = DB::table('subjects')
                            ->select('subjects.*','class.className')
                            ->join('class', 'class.id','=','subjects.classId')
                            ->where('subjects.id', $id)
                            ->get();
        $data = [
            'title' => 'Update class '.$datas[0]->className.' subject '.$datas[0]->subjectName,
            'classData' => $classData,
            'data' => $datas
        ];
        return view('admin.subjects.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'class'=>'required',
            'subject'=>'required'
        ]);
        $test = DB::table('subjects')->where(
            'classId',$request->input('class')
            )
            ->where('subjectName',$request->input('subject'))
            ->get();
        if(sizeOf($test)>0) {
            return redirect('/subjects')->with('error','Class And Subject Already Exists....');
        }
        $subjects = subjects::find($id);
        $subjects->classId = $request->input('class');
        $subjects->subjectName = $request->input('subject');
        $subjects->description = $request->input('description');
        $subjects->save();
        return redirect('/subjects')->with('success','Subject Updated Successfully....');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subjects  $subjects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from teacherTeach where subjectId = ?',[$id]);
        DB::delete('delete from studiedSubjects where subjectId = ?',[$id]);
        DB::delete('delete from result where subjectId = ?',[$id]);
        subjects::find($id)->delete();
        return redirect('/subjects')->with('success','Deleted Successfully');
    }
    public function search(Request $search)
    {
        // return $search;
        $datas = DB::table('subjects')
                            ->select('subjects.*','class.className')
                            ->join('class', 'class.id','=','subjects.classId')
                            ->where('subjects.subjectName','like','%'.$search->get('search').'%')
                            ->orderBy('className')
                            ->paginate(10);
        
        $data = [
            'title' => 'Subject Search Result',
            'datas' => $datas
        ];
        return view('admin.subjects.index')->with('data', $data);
    }
}
