<?php

namespace App\Http\Controllers;

use App\students;
use App\classModel;
use App\sections;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class StudentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        $this->middleware(function($request, $next) {
            if(Auth::user()->status == 2) {
                return redirect('/parent')->with('error','you do not have permission to access this function...');
            } elseif(Auth::user()->status == 1) {
                return redirect('/result')->with('error','you do not have permission to access this function...');
            } else {
                return $next($request);
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = students::paginate(10);;
        $data = [
            'title' => 'Students',
            'datas' => $datas
        ];

        return view('admin.students.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classData = classModel::orderBy('className')->get();
        $data = [
            'title' => 'Add Student',
            'classData' => $classData
        ];
        return view('admin.students.create')->with('data',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'fName'=>'required',
            'lName'=>'required',
            'hNumber'=>'required',
            'streetName'=>'required',
            'cityName' => 'required',
            'state' => 'required',
            'postCodeNumber'=>'required',
            'contactNumber' => 'required',
            'email'=>'required',
            'class'=>'required',
            'section'=>'required'
        ]);
        $check = DB::table('students')
                ->groupBy('sectionId')
                ->count();
        if(! ($check<= 30)) {
            return redirect('/students')->with('error','Section is full. Select new section...');
        }
        $students = new students();
        $students->firstName = $request->input('fName');
        $students->middleName = $request->input('mName');
        $students->lastName = $request->input('lName');
        $students->houseNumber = $request->input('hNumber');
        $students->street = $request->input('streetName');
        $students->city = $request->input('cityName');
        $students->state = $request->input('state');
        $students->postcode = $request->input('postCodeNumber');
        $students->contactNumber = $request->input('contactNumber');
        $students->email = $request->input('email');
        $students->classId = $request->input('class');
        $students->sectionId = $request->input('section');
        $students->save();
        return redirect('/students')->with('success','Student Added...');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\students  $students
     * @return \Illuminate\Http\Response
     */
    public function show(students $students)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\students  $students
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classData = classModel::orderBy('className')->get();
        $studentData = students::find($id);
        $data = [
            'title' => 'Update Student',
            'classData' => $classData,
            'studentData' => $studentData 
        ];
        return view('admin.students.update')->with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\students  $students
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fName'=>'required',
            'lName'=>'required',
            'hNumber'=>'required',
            'streetName'=>'required',
            'cityName' => 'required',
            'state' => 'required',
            'postCodeNumber'=>'required',
            'contactNumber' => 'required',
            'email'=>'required',
            'class'=>'required',
            'section'=>'required'
        ]);
        $check = DB::table('students')
                ->groupBy('sectionId')
                ->count();
        if(! ($check<= 30)) {
            return redirect('/students')->with('error','Section is full. Select new section...');
        }
        $students = students::find($id);
        $students->firstName = $request->input('fName');
        $students->middleName = $request->input('mName');
        $students->lastName = $request->input('lName');
        $students->houseNumber = $request->input('hNumber');
        $students->street = $request->input('streetName');
        $students->city = $request->input('cityName');
        $students->state = $request->input('state');
        $students->postcode = $request->input('postCodeNumber');
        $students->contactNumber = $request->input('contactNumber');
        $students->email = $request->input('email');
        $students->classId = $request->input('class');
        $students->sectionId = $request->input('section');
        $students->save();
        return redirect('/students')->with('success','Successfully Updated...');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\students  $students
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::delete('delete from studiedSubjects where studentId = ?',[$id]);
        DB::delete('delete from parentChild where studentId = ?',[$id]);
        DB::delete('delete from result where studentId = ?',[$id]);
        students::find($id)->delete();
        return redirect('/students')->with('success','Successfully Deleted...');
    }

    public function loadData(Request $request) {
        $data = DB::table('sections')
                ->where('classId', $request->get('id'))
                ->orderBy('sectionName')
                ->get();
                
        return $data;
    }

    public function search(Request $search) {
        $datas = students::where(
            'firstName','like','%'.$search->get('search').'%'
        )->paginate(10);;
        $data = [
            'title' => 'Students',
            'datas' => $datas
        ];

        return view('admin.students.index')->with('data', $data);
    }
}
