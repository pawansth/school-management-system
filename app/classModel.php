<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classModel extends Model
{
    protected $table = 'class';

    public function teacherTeach()
    {
        return $this->hasMany(teacherTeach::class);
    }
    public function sections()
    {
        return $this->hasMany(sections::class);
    }
    public function subjects()
    {
        return $this->hasMany(subjects::class);
    }
    public function students()
    {
        return $this->hasMany('App\students');
    }
}
