<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parentChild extends Model
{
    protected $table = 'parentChild';

    public function signup()
    {
        return $this->belongsTo('App\signup', 'parentId');
    }
    public function students() {
        return $this->belongsTo('App\students', 'studentId');
    }
    public function result()
    {
        return $this->hasMany('App\ResultModel','studentId')->where('classId',$this->students->classId);
    }
}
