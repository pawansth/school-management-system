<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messages extends Model
{
    //
    protected $table = 'messages';

    public function messageByRelation()
    {
        return $this->belongsTo('App\signup', 'messageBy');
    }
    
    public function messageForRelation()
    {
        return $this->belongsTo('App\signup', 'messageFor');
    }
}
