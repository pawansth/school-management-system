<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class teacherTeach extends Model
{
    protected $table = 'teacherTeach';

    public function signup() {
        return $this->belongsTo('App\signup', 'teacherId');
    }

    public function sections() {
        return $this->belongsTo('App\sections', 'sectionId');
    }

    public function subjects() {
        return $this->belongsTo('App\subjects', 'subjectId');
    }
    public function classModel() {
        return $this->belongsTo('App\classModel', 'classId');
    }
}
