<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjects extends Model
{
    protected $table = 'subjects';

    public function teacherTeach()
    {
        return $this->hasOne(teacherTeach::class);
    }
    public function classModel()
    {
        return $this->hasMany(classModel::class);
    }
}
