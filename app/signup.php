<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class signup extends Authenticatable
{
    //
    use Notifiable;
    protected $table = 'signup';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function teacherTeach()
    {
        return $this->hasMany(teacherTeach::class);
    }
    public function parentChild()
    {
        return $this->hasMany('App\parentChild');
    }
}
