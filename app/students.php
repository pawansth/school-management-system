<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class students extends Model
{
    protected $table = 'students';

    public function classModel()
    {
        return $this->belongsTo('App\classModel', 'classId');
    }
    public function sections()
    {
        return $this->belongsTo('App\sections', 'sectionId');
    }
    public function parentChild()
    {
        return $this->hasMany('App\parentChild');
    }
    public function studiedSubjects()
    {
        return $this->hasMany('App\studiedSubjects', 'studentId');
    }
}
