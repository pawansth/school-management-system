<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResultModel extends Model
{
    protected $table = 'result';

    public function students()
    {
        return $this->belongsTo('App\students', 'studentId');
    }
    public function subjects()
    {
        return $this->belongsTo('App\subjects', 'subjectId');
    }
    public function classModel()
    {
        return $this->belongsTo('App\classModel', 'classId');
    }
}
