<?php 
    // dd();
    $title = $data['title'];
    $userId = $data['userId'];
?>


@extends('front.layout.app')

@push('head')

    <script></script>

@endpush

@section('content')

    <div class="teacherListContainer">
        <div class="teacherList">
            @foreach($data['teacherList'] as $teacher)
                @if(sizeof($teacher->students->sections->teacherTeach) > 0)
                <ul class="list-group">
                    @foreach($teacher->students->sections->teacherTeach as $te)
                    <a href="/message-teacher/{{$te->teacherId}}">
                        <li class="list-group-item">
                            <?php
                                $teacherName = $te->signup->firstName;
                                if(!empty($te->signup->middleName)) {
                                    $teacherName = $teacherName.' '.$te->signup->middleName;
                                }
                                $teacherName = $teacherName.' '.$te->signup->lastName.'('.$te->subjects->subjectName.')';
                                echo $teacherName;
                            ?>
                        </li>
                    </a>
                    @endforeach
                </ul>
                @else
                    <div class="jumbotron">
                        No teacher to message....
                    </div>
                @endif
            @endforeach
        </div>
    </div>

@endsection