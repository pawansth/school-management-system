<?php 
    // dd();
    $title = $data['title'];
    $userId = $data['userId'];
?>


@extends('front.layout.app')

@section('content')

    @foreach ($data['children'] as $item)
        <div class="jumbotron">
            <div style="width: 100%; word-wrap: break-word;">
                <p>
                    <span>Student Name: </span>
                    <span>{{ $item->students->firstName }}</span>
                    @if(!empty($item->students->middleName))
                    <span> {{ $item->students->middleName }}</span>
                    @endif
                    <span>{{ $item->students->lastName }}</span>
                </p>
                <p>
                    <span>Class: </span>
                    <span>{{ $item->students->classModel->className }}</span>
                </p>
                <p>
                    <span>Section: </span>
                    <span>{{ $item->students->sections->sectionName }}</span>
                </p>
                <p>
                    <span>Email: </span>
                    <span>{{ $item->students->email }}</span>
                </p>
                <p>
                    <span>Contact Number: </span>
                    <span>{{ $item->students->contactNumber }}</span>
                </p>
                <?php
                $result = $item->result;
                ?>
                @if(sizeof($result)>0)
                @foreach($result as $results)
                    <h4>
                        Term: <span>{{ $results->term }}</span>
                        <span style="font-size: 12px;">({{$results->subjects->subjectName}})</span>
                    </h4>
                    <p>
                        <a target="_balank" href="{{ asset('storage/'.$results->path) }}">
                            {{ asset('storage/'.$results->path) }}
                        </a>
                    </p>
                @endforeach
                @endif
            </div>
        </div>
    @endforeach

@endsection