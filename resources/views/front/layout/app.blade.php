<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $title ?? 'Student Management System' }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
        <script src="{{ asset('js/app.js') }}"></script>
        @stack('head')
    </head>
    <body>
        
        <div class="container-fluid">
                <!-- Side navigation -->
            <div class="nav nav-tabs btn-info">
                <li class="nav-item">
                    <a class="nav-link" style="color: whitesmoke;" href="/parent">
                        Home
                    </a>
                {{-- </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: whitesmoke;" href="#">
                        Notification
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" style="color: whitesmoke;" href="/teacher-list/{{$userId}}">
                        Message
                    </a>
                </li>
                <form id="frm-logout" action="/logout" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="nav-item btn btn-default" style="
                    padding: 6px 8px 6px 16px;
                    font-size: 16px !important;
                    color: whitesmoke;;
                    ">Logout</button>
                </form>
            </div>
            <!-- navigation bar ends here -->


            <div style="height: 100vh; width: 50%; margin-left: auto; margin-right: auto;">
                <!-- Content container -->
                <div class="content">
                    @include('admin.layout.message')
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
