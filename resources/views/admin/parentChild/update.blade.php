<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/parent-child/{{$data['datas']->id}}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            
            <div class="form-group">
                <label for="parent">Parent Name</label>
                <select name="parent" id="parent" class="form-control">
                        @foreach ($data['parentData'] as $item)
                        @if($item->id == $data['datas']->parentId)
                        <option value="{{$item->id}}" selected>
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @else
                        <option value="{{$item->id}}">
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @endif
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="child">Child Name</label>
                <select name="child" id="child" class="form-control">
                        @foreach ($data['studentData'] as $item)
                        @if($item->id == $data['datas']->studentId)
                        <option value="{{$item->id}}" selected>
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @else
                        <option value="{{$item->id}}">
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @endif
                        @endforeach
                </select>
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection