<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/parent-child') }}" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="parent">Parent Name</label>
                <select name="parent" id="parent" class="form-control">
                        @foreach ($data['parentData'] as $item)
                            <option value="{{$item->id}}">
                                <?php
                                $name = $item->firstName;
                                if(!empty($item->middleName))
                                $name = $name.' '.$item->middleName;
                                $name = $name.' '.$item->lastName;
                                echo $name;
                                ?>
                            </option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="child">Child Name</label>
                <select name="child" id="child" class="form-control">
                        @foreach ($data['studentData'] as $item)
                            <option value="{{$item->id}}">
                                <?php
                                $name = $item->firstName;
                                if(!empty($item->middleName))
                                $name = $name.' '.$item->middleName;
                                $name = $name.' '.$item->lastName;
                                echo $name;
                                ?>
                            </option>
                        @endforeach
                </select>
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection