
<div style="margin-top: 10px; margin-bottom:10px;">
    @if ($errors->any())
<div class="alert alert-danger">
        @foreach ($errors->all() as $error)
            {{ $error }}
            <br>
        @endforeach
</div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

</div>