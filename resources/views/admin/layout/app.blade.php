<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $title ?? 'Student Management System' }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
        <script src="{{ asset('js/app.js') }}"></script>
        @stack('head')
    </head>
    <body>
        
        <div class="container-fluid">
                <!-- Side navigation -->
            <div class="sidenav">
                <a href="/register">Registration</a>
                <a href="/class">Class</a>
                <a href="/sections">Sections</a>
                <a href="/subjects">Subjects</a>
                <a href="/teach">Teaching Subjects</a>
                <a href="/students">Students</a>
                <a href="/studied-subjects">Studying Subjects</a>
                <a href="/parent-child">Parent's Children</a>
                <a href="/result">Result</a>
                <a href="/teacher-message-view">Message</a>
                <form id="frm-logout" action="/logout" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-default" style="
                    padding: 6px 8px 6px 16px;
                    font-size: 16px !important;
                    color: #818181;
                    ">Logout</button>
                </form>
            </div>
            <!-- navigation bar ends here -->


            <div id="main-content-container">
                <!-- Content container -->
                <div class="content">
                    @include('admin.layout.message')
                    @yield('content')
                </div>
            </div>
        </div>
    </body>
</html>
