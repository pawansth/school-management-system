<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@push('head')
        <script>
            var sectionId = {{$data['datas'][0]->sectionId}};
            var subjectId = {{$data['datas'][0]->subjectId}};
            $(window).on('load', function() {
                $.ajax({url: "/teach-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    // console.log(result);
                                    $.each(result['sectionData'], function(index, value) {
                                        if(value.id == sectionId) {
                                            $(document).find('#section').append(
                                            '<option value="'+value.id+'" selected>'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                        } else {
                                            $(document).find('#section').append(
                                            '<option value="'+value.id+'">'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                        }
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        if(value.id == subjectId) {
                                            $(document).find('#subject').append(
                                            '<option value="'+value.id+'" selected>'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                        } else {
                                            $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                        }
                                    });
                    }});
            });
            function loadData() {
                $.ajax({url: "/teach-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    console.log(result);
                                    $.each(result['sectionData'], function(index, value) {
                                        $(document).find('#section').append(
                                            '<option value="'+value.id+'">'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                    });
                    }});
            }
        </script>
    @endpush

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/teach/{{$data['datas'][0]->id}}" method="POST">
        {{method_field('PUT')}}    
        @csrf
            <div class="form-group">
                <label for="teacher">Teacher Name</label>
                <select name="teacher" id="teacher" class="form-control">
                        @foreach ($data['teacherData'] as $item)
                        @if ($item->id == $data['datas'][0]->teacherId)
                        <option value="{{$item->id}}" selected>
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @else
                        <option value="{{$item->id}}">
                            <?php
                            $name = $item->firstName;
                            if(!empty($item->middleName))
                            $name = $name.' '.$item->middleName;
                            $name = $name.' '.$item->lastName;
                            echo $name;
                            ?>
                        </option>
                        @endif
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="class">Class Name</label>
            <select name="class" id="class" class="form-control" onchange="loadData()">
                    <option>---Chose a class---</option>
                        @foreach ($data['classData'] as $item)
                        @if ($item->id == $data['datas'][0]->classId)
                            <option value="{{$item->id}}" selected>{{$item->className}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endif
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="section">Section Name</label>
                <select name="section" id="section" class="form-control">
                </select>
            </div>
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <select name="subject" id="subject" class="form-control">
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection