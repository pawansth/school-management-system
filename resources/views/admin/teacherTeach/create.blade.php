<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@push('head')
        <script>
            function loadData() {
                $.ajax({url: "/teach-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    console.log(result);
                                    $.each(result['sectionData'], function(index, value) {
                                        $(document).find('#section').append(
                                            '<option value="'+value.id+'">'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                    });
                    }});
            }
        </script>
    @endpush

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/teach') }}" method="POST">
            @csrf
            
            <div class="form-group">
                <label for="teacher">Teacher Name</label>
                <select name="teacher" id="teacher" class="form-control">
                        @foreach ($data['teacherData'] as $item)
                            <option value="{{$item->id}}">
                                <?php
                                $name = $item->firstName;
                                if(!empty($item->middleName))
                                $name = $name.' '.$item->middleName;
                                $name = $name.' '.$item->lastName;
                                echo $name;
                                ?>
                            </option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="class">Class Name</label>
            <select name="class" id="class" class="form-control" onchange="loadData()">
                    <option>---Chose a class---</option>
                        @foreach ($data['classData'] as $item)
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="section">Section Name</label>
                <select name="section" id="section" class="form-control">
                </select>
            </div>
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <select name="subject" id="subject" class="form-control">
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection