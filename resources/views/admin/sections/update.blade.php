<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/sections/{{$data['data'][0]->id}}" method="POST">
        {{method_field('PUT')}}
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <select name="class" id="class" class="form-control">
                        @foreach ($data['classData'] as $item)
                        @if($data['data'][0]->classId == $item->id)
                        <option value="{{$item->id}}" selected>{{$item->className}}</option> 
                        @else
                        <option value="{{$item->id}}">{{$item->className}}</option>
                        @endif
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="section">Section Name</label>
                <input type="text" value="{{$data['data'][0]->sectionName}}" id="section" name="section" class="form-control" required placeholder="Section Name">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection