<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/sections') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <select name="class" id="class" class="form-control">
                        @foreach ($data['classData'] as $item)
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="section">Section Name</label>
                <input type="text" id="section" name="section" class="form-control" required placeholder="Section Name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection