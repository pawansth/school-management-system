<?php

 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/class/{{$data['data']['id']}}" method="POST">
        {{method_field('PUT')}}
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <input type="number" value="{{ $data['data']['className'] }}" id="class" name="class" class="form-control" required placeholder="Class Name">
            </div>
            <button type="Update" class="btn btn-info">Update</button>
        </form>
    </div>
@endsection