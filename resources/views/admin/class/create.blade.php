<?php

 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/class') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <input type="number" id="class" name="class" class="form-control" required placeholder="Class Name">
            </div>
            <button type="Submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection