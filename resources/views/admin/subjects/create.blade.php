<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/subjects') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <select name="class" id="class" class="form-control">
                        @foreach ($data['classData'] as $item)
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <input type="text" id="subject" name="subject" class="form-control" required placeholder="subject Name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control" rows="10" cols="50" placeholder="Write Some thing about a subject...."></textarea>
            </div>
            <button type="Submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection