<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/subjects/{{$data['data'][0]->id}}" method="POST">
        {{method_field('PUT')}}
            @csrf
            <div class="form-group">
                <label for="class">Class Name</label>
                <select name="class" id="class" class="form-control">
                        @foreach ($data['classData'] as $item)
                        @if($data['data'][0]->classId == $item->id)
                        <option value="{{$item->id}}" selected>{{$item->className}}</option> 
                        @else
                        <option value="{{$item->id}}">{{$item->className}}</option>
                        @endif
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <input type="text" value="{{$data['data'][0]->subjectName}}" id="subject" name="subject" class="form-control" required placeholder="Subject Name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control" cols="50" rows="10" placeholder="Write Some thing about a subject....">{{$data['data'][0]->description}}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection