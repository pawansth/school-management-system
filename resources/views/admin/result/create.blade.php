<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@push('head')
        <script>

            function loadData() {
                $.ajax({url: "/studied-subjects-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    // console.log(result);
                                    
                                    $.each(result['studentsData'], function(index, value) {
                                        var studentName = value.firstName;
                                        if(value.middleName != '' || value.middleName != null || value.middleName != undefined) {
                                                    studentName = studentName+' '+value.middleName;
                                                }
                                        studentName = studentName+' '+value.lastName;
                                        $(document).find('#student').append(
                                            '<option value="'+value.id+'">'+
                                                studentName
                                                +'</option>'
                                            );
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                    });
                    }});
            }
        </script>
    @endpush

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="{{ url('/result') }}" method="POST" enctype="multipart/form-data">
            @csrf
            
            <div class="form-group">
                <label for="class">Class Name</label>
            <select name="class" id="class" class="form-control" onchange="loadData()">
                    <option>---Chose a class---</option>
                        @foreach ($data['classData'] as $item)
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="student">Student Name</label>
                <select name="student" id="student" class="form-control">
                </select>
            </div>
            
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <select name="subject" id="subject" class="form-control">
                </select>
            </div>

            <div class="form-group">
                <label for="term">Term</label>
                <input type="number" id="term" name="term" class="form-control" placeholder="Enter term in number..." max="4">
            </div>

            <div class="form-group d-flex flex-column">
                <label for="result">Result 
                    <span style="color: red;">.pdf and upto 5 MB file only</span>
                </label>
                <input type="file" id="result" name="result" class="py-2">
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection