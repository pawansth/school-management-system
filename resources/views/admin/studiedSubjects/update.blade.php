<?php
 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@push('head')
        <script>
            var studentId = {{$data['datas'][0]->studentId}};
            var subjectId = {{$data['datas'][0]->subjectId}};
            $(window).on('load', function() {
                $.ajax({url: "/studied-subjects-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    // console.log(result);
                                    $.each(result['studentsData'], function(index, value) {
                                        var studentName = value.firstName;
                                            if(value.middleName != '' || value.middleName != null || value.middleName != undefined) {
                                                        studentName = studentName+' '+value.middleName;
                                                    }
                                            studentName = studentName+' '+value.lastName;
                                        if(value.id == studentId) {
                                            $(document).find('#student').append(
                                            '<option value="'+value.id+'" selected>'+
                                                studentName
                                                +'</option>'
                                            );
                                        } else {
                                            $(document).find('#student').append(
                                            '<option value="'+value.id+'">'+
                                                studentName
                                                +'</option>'
                                            );
                                        }
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        if(value.id == subjectId) {
                                            $(document).find('#subject').append(
                                            '<option value="'+value.id+'" selected>'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                        } else {
                                            $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                        }
                                    });
                    }});
            });
            function loadData() {
                $.ajax({url: "/studied-subjects-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    // console.log(result);
                                    
                                    $.each(result['studentsData'], function(index, value) {
                                        var studentName = value.firstName;
                                        if(value.middleName != '' || value.middleName != null || value.middleName != undefined) {
                                                    studentName = studentName+' '+value.middleName;
                                                }
                                        studentName = studentName+' '+value.lastName;
                                        $(document).find('#student').append(
                                            '<option value="'+value.id+'">'+
                                                studentName
                                                +'</option>'
                                            );
                                    });
                                    $.each(result['subjectData'], function(index, value) {
                                        $(document).find('#subject').append(
                                            '<option value="'+value.id+'">'+
                                                value.subjectName
                                                +'</option>'
                                            );
                                    });
                    }});
            }
        </script>
    @endpush

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/studied-subjects/{{$data['datas'][0]->id}}" method="POST">
        {{method_field('PUT')}}    
        @csrf

            <div class="form-group">
                <label for="class">Class Name</label>
            <select name="class" id="class" class="form-control" onchange="loadData()">
                    <option>---Chose a class---</option>
                        @foreach ($data['classData'] as $item)
                        @if ($item->id == $data['datas'][0]->classId)
                            <option value="{{$item->id}}" selected>{{$item->className}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->className}}</option>
                        @endif
                        @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="student">Student Name</label>
                <select name="student" id="student" class="form-control">
                </select>
            </div>
            
            <div class="form-group">
                <label for="subject">Subject Name</label>
                <select name="subject" id="subject" class="form-control">
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection