<?php 
    // dd();
    $title = $data['title'];
?>


@extends('admin.layout.app')

@push('head')

    <script></script>

@endpush

@section('content')
    <div class="parentsContainer">
        {{-- {{dd($data['messages'])}} --}}
        <ul class="list-group">
            @foreach ($data['messages'] as $item)
                <a href="/teacher-message/{{$item->messageBy}}">
                    <li class="list-group-item">
                        <?php
                            $parentName = $item->messageByRelation->firstName;
                            if(!empty($te->messageByRelation->middleName)) {
                                $parentName = $parentName.' '.$item->messageByRelation->middleName;
                            }
                            $parentName = $parentName.' '.$item->messageByRelation->lastName;
                            echo $parentName;
                        ?>
                    </li>
                </a>
            @endforeach
        </ul>
    </div>
@endsection