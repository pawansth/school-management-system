<?php 
    // dd();
    $title = $data['title'];
?>


@extends('admin.layout.app')

@push('head')

    <script></script>

@endpush

@section('content')

    <div class="messageContainer" style="
    width: 50%;
    height:50vh;
    overflow-y: auto;
    border: 1px solid;
    margin-left: auto;
    margin-right: auto;
    ">
        {{-- {{dd($data['messages'])}} --}}
        @foreach ($data['messages'] as $message)
            @if($message->messageBy == $data['userId'])
                <div class="messagesBy" style="
                display: inline-block;
                ">
                    <div class="user" style="
                    float: left;
                    margin-top: 10px;
                    margin-left: 10px;
                    border: 1px solid black;
                    padding:10px;
                    border-radius: 50%;
                    ">
                        <h5>You</h5>
                    </div>
                    <div class="messageContent" style="
                    float: left; word-break: break-all;
                    margin-top: 15px;
                    margin-left: 10px;
                    ">
                        {{$message->message}}
                    </div>
                </div>
                <br/>
            @else
                <div class="messagesFor" style="
                display: inline-block;
                ">
                    <div class="user" style="
                    float: left;
                    margin-top: 10px;
                    margin-left: 10px;
                    border: 1px solid black;
                    padding:10px;
                    border-radius: 50%;
                    ">
                        <h5>{{$message->messageByRelation->firstName}}</h5>
                    </div>
                    <div class="messageContent" style="
                    float: left; word-break: break-all;
                    margin-top: 15px;
                    margin-left: 10px;
                    ">
                        {{$message->message}}
                    </div>
                </div>
                <br/>
            @endif
        @endforeach
    </div>
    <div class="sendMessage" style="
    width: 50%;
    margin-left: auto;
    margin-right: auto;
    ">
        <form action="/send-message-parent" method="POST">
            @csrf
            <input type="hidden" value="{{$data['parentId']}}" name="parentId">
            <textarea name="messageToTeacher" id="messageToTeacher" style="margin-top: 10px;" class="form-control" cols="30" rows="4" placeholder="Type your message..."></textarea>
            <button type="submit" class="btn btn-primary" style="margin: 10px;">Send</button>
        </form>
    </div>

@endsection