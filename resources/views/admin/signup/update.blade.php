<?php

 $title = $data['title'];

 ?>
@extends('admin.layout.app')


@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/register/{{$data['data']['id']}}" method="POST">
        {{method_field('PUT')}}
            @csrf
            <div class="form-group">
                <label for="fName">First Name</label>
                <input type="text" value="{{ $data['data']['firstName'] }}" id="fName" name="fName" class="form-control" required placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="mName">Middle Name</label>
                <input type="text" value="{{ $data['data']['middleName'] }}" id="mName" name="mName" class="form-control" placeholder="Middle Name">
            </div>
            <div class="form-group">
                <label for="lName">Last Name</label>
                <input type="text" value="{{ $data['data']['lastName'] }}" id="lName" name="lName" class="form-control" required placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="hNumber">House Number</label>
                <input type="text" value="{{ $data['data']['houseNumber'] }}" id="hNumber" name="hNumber" class="form-control" required placeholder="House Number">
            </div>
            <div class="form-group">
                <label for="streetName">Street</label>
                <input type="text" value="{{ $data['data']['street'] }}" id="streetName" name="streetName" class="form-control" required placeholder="Street">
            </div>
            <div class="form-group">
                <label for="cityName">City</label>
                <input type="text" value="{{ $data['data']['city'] }}" id="cityName" name="cityName" class="form-control" required placeholder="City">
            </div>
            <div class="form-group">
                <label for="stateName">State</label>
                <input type="text" value="{{ $data['data']['state'] }}" id="stateName" name="state" class="form-control" required placeholder="State">
            </div>
            <div class="form-group">
                <label for="postcodeNumber">PostCode</label>
                <input type="number" value="{{ $data['data']['postcode'] }}" id="postCodeNumber" name="postCodeNumber" class="form-control" required placeholder="Postcode">
            </div>
            <div class="form-group">
                <label for="contactNumber">Contact Number</label>
                <input type="number" value="{{ $data['data']['contactNumber'] }}" id="contactNumber" name="contactNumber" class="form-control" required placeholder="Contact Number">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" value="{{ $data['data']['email'] }}" id="email" name="email" class="form-control" required placeholder="Email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" value="{{ $data['data']['password'] }}" id="password" name="password" class="form-control" required placeholder="Password">
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select value="{{ $data['data']['status'] }}" class="form-control" id="status" name="status" required>
                    <option value="0">Admin</option>
                    <option value="1">Teacher</option>
                    <option value="2">Parent</option>
                  </select>
            </div>
            <button type="submit" class="btn btn-info">Update</button>
        </form>
    </div>
@endsection