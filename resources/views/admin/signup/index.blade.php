<?php

 $title = $data['title'];

 ?>

@extends('admin.layout.app')

@section('content')

    <div class="signup-container">
    <h1>{{ $data['title'] }}</h1>
        <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
            <div class="header col-md-6">
                <a href="register/create" class="addNew btn btn-primary" id="newRegister">
                    <svg class="bi bi-person-plus" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M11 14s1 0 1-1-1-4-6-4-6 3-6 4 1 1 1 1h10zm-9.995-.944v-.002.002zM1.022 13h9.956a.274.274 0 00.014-.002l.008-.002c-.001-.246-.154-.986-.832-1.664C9.516 10.68 8.289 10 6 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664a1.05 1.05 0 00.022.004zm9.974.056v-.002.002zM6 7a2 2 0 100-4 2 2 0 000 4zm3-2a3 3 0 11-6 0 3 3 0 016 0zm4.5 0a.5.5 0 01.5.5v2a.5.5 0 01-.5.5h-2a.5.5 0 010-1H13V5.5a.5.5 0 01.5-.5z" clip-rule="evenodd"/>
                        <path fill-rule="evenodd" d="M13 7.5a.5.5 0 01.5-.5h2a.5.5 0 010 1H14v1.5a.5.5 0 01-1 0v-2z" clip-rule="evenodd"/>
                      </svg>
                    Add
                </a>
            </div>
            <div class="col-md-6">
                <form action="/register-search" method="GET">
                <div class="input-group">
                    <input type="text" name="search" placeholder="Search by first name...." class="form-control">
                <div class="input-group-append">
                    <button type="submit" class="btn btn-success">
                        <svg class="bi bi-search" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 011.415 0l3.85 3.85a1 1 0 01-1.414 1.415l-3.85-3.85a1 1 0 010-1.415z" clip-rule="evenodd"/>
                            <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 100-11 5.5 5.5 0 000 11zM13 6.5a6.5 6.5 0 11-13 0 6.5 6.5 0 0113 0z" clip-rule="evenodd"/>
                          </svg>
                        Search
                    </button>
                </div>
                </div>
                </form>
            </div>
        </div>
        <div class="body">
            
                    @if (count($data['datas']) > 0)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">S.N</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Middle Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">House Number</th>
                                    <th scope="col">Street</th>
                                    <th scope="col">City</th>
                                    <th scope="col">State</th>
                                    <th scope="col">PostCode</th>
                                    <th scope="col">Contact</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Password</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach ($data['datas'] as $item)
                            <tr>
                            <th>{{$i}}</th>
                                <td>{{$item->firstName}}</td>
                                <td>{{$item->middleName}}</td>
                                <td>{{$item->lastName}}</td>
                                <td>{{$item->houseNumber}}</td>
                                <td>{{$item->street}}</td>
                                <td>{{$item->city}}</td>
                                <td>{{$item->state}}</td>
                                <td>{{$item->postcode}}</td>
                                <td>{{$item->contactNumber}}</td>
                                <td>{{$item->email}}</td>
                                <td>{{$item->password}}</td>
                                <td>
                                    @if ($item->status == 0)
                                        Admin
                                    @elseif ($item->status == 1)
                                        Teacher
                                    @else
                                        Parent
                                    @endif
                                </td>
                                <td>
                                    {{-- <a href="" class="btn btn-primary btn-sm float-left" style="display: inline-block;
                                    vertical-align: top;
                                    margin: 1px 1px;">
                                        <svg class="bi bi-eye" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 001.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0014.828 8a13.133 13.133 0 00-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 001.172 8z" clip-rule="evenodd"/>
                                            <path fill-rule="evenodd" d="M8 5.5a2.5 2.5 0 100 5 2.5 2.5 0 000-5zM4.5 8a3.5 3.5 0 117 0 3.5 3.5 0 01-7 0z" clip-rule="evenodd"/>
                                          </svg>
                                    </a> --}}
                                    <a href="register/{{ $item->id }}/edit" class="btn btn-info btn-sm btn-edit" style="display: inline-block;
                                    vertical-align: top;
                                    margin: 1px 1px;">
                                        <svg class="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M11.293 1.293a1 1 0 011.414 0l2 2a1 1 0 010 1.414l-9 9a1 1 0 01-.39.242l-3 1a1 1 0 01-1.266-1.265l1-3a1 1 0 01.242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z" clip-rule="evenodd"/>
                                            <path fill-rule="evenodd" d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 00.5.5H4v.5a.5.5 0 00.5.5H5v.5a.5.5 0 00.5.5H6v-1.5a.5.5 0 00-.5-.5H5v-.5a.5.5 0 00-.5-.5H3z" clip-rule="evenodd"/>
                                          </svg>
                                    </a>
                                <form 
                                action="register/{{ $item->id }}" 
                                method="POST" onsubmit="return confirm('are you sure?');" 
                                style="display: inline-block;
                                    vertical-align: top;
                                    margin: 1px 1px;">
                                    {{method_field('DELETE')}}
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <svg class="bi bi-trash-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M2.5 1a1 1 0 00-1 1v1a1 1 0 001 1H3v9a2 2 0 002 2h6a2 2 0 002-2V4h.5a1 1 0 001-1V2a1 1 0 00-1-1H10a1 1 0 00-1-1H7a1 1 0 00-1 1H2.5zm3 4a.5.5 0 01.5.5v7a.5.5 0 01-1 0v-7a.5.5 0 01.5-.5zM8 5a.5.5 0 01.5.5v7a.5.5 0 01-1 0v-7A.5.5 0 018 5zm3 .5a.5.5 0 00-1 0v7a.5.5 0 001 0v-7z" clip-rule="evenodd"/>
                                          </svg>
                                    </button>
                                </form>
                                </td>
                            </tr>
                            <?php $i++;?>
                        @endforeach
                                </tbody>
                            </thead>
                        </table>
                        {{$data['datas']->links()}}
                    @else
                        <div class="jumbotron">
                            There is no data to display.
                        </div>
                    @endif
        </div>
    </div>

@endsection