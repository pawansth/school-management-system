<?php

 $title = $data['title'];

 ?>
@extends('admin.layout.app')

@push('head')
        <script>
            var sectionId = {{$data['studentData']->sectionId}}
            // console.log(sectionId);
            $(window).on('load', function() {
                $.ajax({url: "/students-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    $.each(result, function(index, value) {
                                        if(value.id == sectionId) {
                                            $(document).find('#section').append(
                                            '<option value="'+value.id+'" selected>'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                        } else {
                                            $(document).find('#section').append(
                                            '<option value="'+value.id+'">'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                        }
                                    });
                    }});
            });
            function loadData() {
                $.ajax({url: "/students-load-data",
                        data:{'id':$("select#class").val()},
                        success: function(result){
                                    $.each(result, function(index, value) {
                                        $(document).find('#section').append(
                                            '<option value="'+value.id+'">'+
                                                value.sectionName
                                                +'</option>'
                                            );
                                    });
                    }});
            }
        </script>
    @endpush

@section('content')
    <div class="add-container">
        <h1>{{ $data['title'] }}</h1>
    </div>
    <div class="form-container form-holder">
    <form action="/students/{{$data['studentData']->id}}" method="POST">
        {{method_field('PUT')}}
            @csrf
            <div class="form-group">
                <label for="fName">First Name</label>
            <input type="text" value="{{$data['studentData']->firstName}}" id="fName" name="fName" class="form-control" required placeholder="First Name">
            </div>
            <div class="form-group">
                <label for="mName">Middle Name</label>
            <input type="text" value="{{$data['studentData']->middleName}}" id="mName" name="mName" class="form-control" placeholder="Middle Name">
            </div>
            <div class="form-group">
                <label for="lName">Last Name</label>
            <input type="text" value="{{$data['studentData']->lastName}}" id="lName" name="lName" class="form-control" required placeholder="Last Name">
            </div>
            <div class="form-group">
                <label for="hNumber">House Number</label>
            <input type="text" value="{{$data['studentData']->houseNumber}}" id="hNumber" name="hNumber" class="form-control" required placeholder="House Number">
            </div>
            <div class="form-group">
                <label for="streetName">Street</label>
            <input type="text" value="{{$data['studentData']->street}}" id="streetName" name="streetName" class="form-control" required placeholder="Street">
            </div>
            <div class="form-group">
                <label for="cityName">City</label>
            <input type="text" value="{{$data['studentData']->city}}" id="cityName" name="cityName" class="form-control" required placeholder="City">
            </div>
            <div class="form-group">
                <label for="stateName">State</label>
            <input type="text" value="{{$data['studentData']->state}}" id="stateName" name="state" class="form-control" required placeholder="State">
            </div>
            <div class="form-group">
                <label for="postcodeNumber">PostCode</label>
            <input type="number" value="{{$data['studentData']->postcode}}" id="postCodeNumber" name="postCodeNumber" class="form-control" required placeholder="Postcode">
            </div>
            <div class="form-group">
                <label for="contactNumber">Contact Number</label>
            <input type="number" value="{{$data['studentData']->contactNumber}}" id="contactNumber" name="contactNumber" class="form-control" required placeholder="Contact Number">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
            <input type="email" value="{{$data['studentData']->email}}" id="email" name="email" class="form-control" required placeholder="Email">
            </div>
            <div class="form-group">
                <label for="class">Class</label>
                <select class="form-control" id="class" name="class" onchange="loadData()" required>
                    <option>--Chose Class---</option>
                    @foreach ($data['classData'] as $item)
                    @if($item->id == $data['studentData']->classId)
                        <option value="{{$item->id}}" selected>{{$item->className}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->className}}</option>
                    @endif
                    <option value="{{$item->id}}">{{$item->className}}</option>
                    @endforeach
                  </select>
            </div>
            <div class="form-group">
                <label for="section">Section</label>
                <select class="form-control" id="section" name="section" required>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection
